<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListModel;

class ListController extends Controller
{
    public function create(Request $request)
    {
        $list = new ListModel();
        $list->title = $request->all()['title'];
        $list->save();
    }

    public function get(Request $request, $id)
    {
        $list = ListModel::query()->find($id);
        return $list;
    }

    public function delete()
    {

    }

    public function patch()
    {

    }
}
