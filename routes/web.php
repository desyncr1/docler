<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/list', 'ListController@create');
$router->get('/list/{id}', 'ListController@get');
$router->delete('/list/{id}', 'ListController@delete');
$router->patch('/list/{id}', 'ListController@patch');

