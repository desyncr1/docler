# TODO list REST API

This is a simple todo list-like project for the Docler interview.

# What is does

- Can create a 'list' and get it's details
- Get and list tasks on a given list (pending)

That's pretty much it.

# TODO

- I'd refactor the ListController to rely on a service layer (ListService, TaskService)
- I'd use a Resource class to serialize and represent the entities
- Add functional and unit testing (with behat and phpunit)

# Installing it

    $ composer install
    
# Running it

    $ php ./artisan serve

# Models

Currently there are 2 models:

- Task
	- id (int unsigned autoincremental)
	- list_id (int unsigned autoincremental)
	- title (max:256)
	- done (1 or 0 -> true or false)
	- created_at timestamp
	- updated_at timestamp

- List
	- id (int unsigned autoincremental)
	- title (max:256)
	- created_at timestamp
	- updated_at timestamp

API
---

Prefix: v1

# List

- POST /list # create an empty list

```
{
    'title': "Sample list"
}
```

- GET /list/{id}	# get list by id

- DELETE /list/{id}

- PATCH /list/{id}

```
{
	'title': 'Updated title'
}
```

# Task

- POST /list/{id}/task # create a task on list with {id}

```
{
	'title': "Sample list",
	'done': false|true
}
```

- GET /list/{id}/tasks	# get tasks from list with id = {id}

- PATCH /list/{list_id}/task/{task_id}

```
{
	'done': true|false
}
```

- DELETE /list/{list_id}/task/{task_id}

